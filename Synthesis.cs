﻿using System;
using System.Collections.Generic;

namespace GradientDescentApplication
{
    public class Synthesis
    {
        //количество уравнений
        private int M;
        //количество шагов
        private int N;
        //коэффициент фи для функции качества
        private int fi;
        //коэффициент пси для функции качества
        private int psi;
        //коэффициенты перед x в системе уравнений
        private double[] A;
        //коэффициенты перед u в системе уравнений
        private double[] B;
        //массив для хранения значений х
        private double[,] x;
        //массив для хранения значений u
        private double[,] u;
        //погрешность методов оптимизации
        private double eps = 0.01;

        public Synthesis(double[] A, double[] B, double[] x0, double[] u0, int N, int fi, int psi)
        {
            this.A = A;
            this.B = B;
            this.N = N;
            this.fi = fi;
            this.psi = psi;
            this.M = A.Length;
            u = new double[M, N];
            x = new double[M, N];
            for (int i = 0; i < M; ++i)
            {
                u[i, 0] = u0[i];
                x[i, 0] = x0[i];
            }
        }

        //получить вектор иксов по индексу через значения u
        private double[] GetX(int index, int n)
        {
            double[] xi = new double[M];
            for (int i = 0; i < xi.Length; ++i)
            {
                double sum = 0;
                for (int j = 0; j < n; ++j)
                {
                    sum += Math.Pow(A[i], index - j - 1) * B[i] * u[i, j];
                }
                xi[i] = Math.Pow(A[i], index) * x[i, 0] + sum;
            }
            return xi;
        }

        //вычисление функции качества
        private double GetV0(int n)
        {
            double v = 0;
            for (int i = 0; i < n; ++i)
            {
                double[] x = GetX(i, n);
                for (int j = 0; j < M; ++j)
                {
                    v += x[j] * x[j] * fi + u[j, i] * u[j, i] * psi;
                }
            }
            return v;
        }

        //функция, используемая в методах оптимизации,
        //для удобства вычисления функции качества
        private double func(double value, int i, int j)
        {
            u[i, j] = value;
            return GetV0(N);
        }

        //выбор изначального интерфала для метода золотого сечения
        private double[] BeginInterval(int i, int j)
		{
			double del = 3.0, a2 = 1.0, a1, a3;
			double f1, f2, f3;
		    a1 = a2 - del;
			a3 = a2 + del;
			f2 = func(a2, i, j);
			f1 = func(a1, i, j);
			f3 = func(a3, i, j);
			while (true)
			{
				if (f1 < f3)
				{
					if (f1 < f2)
					{
						a3 = a2;
						a2 = a1;
						f3 = f2;
						f2 = f1;
						a1 = a2 - del;
					}
					else break;
				}
				else
				{
					if (f2 > f3)
					{
						a1 = a2;
						a2 = a3;
						f1 = f2;
						f2 = f3;
						a3 = a2 + del;
					}
					else break;
				}
			}
            return new double[]{a1, a3};
		}

        //метод золотого сечения для вычисления минимума одномерной функции
        private double GoldSectionSearch(int i, int j)
        {
            double kor = 2.236067977499789696409;
            double[] interval = BeginInterval(i, j);
            double a = interval[0], b = interval[1];
			double x1, x2, y1, y2;
			double delta;
			while (b - a >= eps)
			{
				x2 = a + (b-a) / ((1+kor) / 2);
			    x1 = a + b - x2;
				y1 = func(x1, i , j);
				y2 = func(x2, i , j);
				if (y1 < y2)
				{
					delta = x2 - x1;
					b = x2;
					x2 = x1;
					x1 = a + delta;
					y2 = y1;
				}
				else if (y2 < y1)
				{
					delta = x2 - x1;
					a = x1;
					x1 = x2;
					x1 = b - delta;
					y1 = y2;
				}
				else
				{
					a = x1;
					b = x2;
					x1 = (a + b) / 2 - kor * (b - a) / 2;
					x2 = (a + b) / 2 + kor * (b - a) / 2;
				}
			}
			return a;
        }

        //покоординатный спуск для вычисления минимума многомерной функции
        public List<double> CoordinateDescent()
        {
            List<double> results = new List<double>();
            for (int i = 0; i < u.GetLength(0); ++i)
            {
                for (int j = 0; j < u.GetLength(1); ++j)
                {
                    u[i, j] = 1;
                }
            }
            
            int count = 0;
            while(true) {
                for (int i = 0; i < u.GetLength(1); ++i)
                {
                    for (int j = 0; j < u.GetLength(0); ++j)
                    {
                        u[j, i] = GoldSectionSearch(j, i);
                    }
                }
                results.Add(GetV0(N));
                ++count;
                if (results.Count > 2 && Math.Abs(results[results.Count - 2] - results[results.Count - 1]) < eps || count >= 1000)
                {
                    break;
                }
            }
            return results;
        }

        //получить допустимое управление
        public double[,] GetUValues() {
                return u;
        }

        public double[,] GetXValues() {
            for (int i = 1; i < x.GetLength(1); ++i)
            {
                double[] xi = GetX(i, N);
                for (int j = 0; j < xi.Length; ++j)
                {
                    x[j, i] = xi[j];
                }
            }
            return x;
        }
    }

    //public class Program
    //{
    //    //public static void Main(string[] args)
    //    //{
    //    //    Synthesis syn = new Synthesis(
    //    //        new double[]{0.1, 1.3},
    //    //        new double[]{0.5, 1},
    //    //        new double[]{1, 1},
    //    //        new double[]{1, 1},
    //    //        3, 1, 1
    //    //    );
    //    //    List<double> res = syn.CoordinateDescent();
    //    //    res.ForEach(i => System.Console.WriteLine(i));
    //    //    Console.WriteLine();
    //    //    double[,] UValues = syn.GetUValues();
    //    //    for (int i = 0; i < UValues.GetLength(0); i++)
    //    //    {
    //    //        for (int k = 0; k < UValues.GetLength(1); k++)
    //    //        {
    //    //            Console.Write(UValues[i,k] + "; ");
    //    //        }
    //    //        Console.WriteLine();
    //    //    }
    //    //    Console.WriteLine();
    //    //    double[,] XValues = syn.GetXValues();
    //    //    for (int i = 0; i < XValues.GetLength(0); i++)
    //    //    {
    //    //        for (int k = 0; k < XValues.GetLength(1); k++)
    //    //        {
    //    //            Console.Write(XValues[i,k] + "; ");
    //    //        }
    //    //        Console.WriteLine();
    //    //    }
    //    //}
    //}
}
